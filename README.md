##### Semana 1 - Empezando con spring-boot. -
# Spring boot sample

"Hola Mundo" RESTful web service con Spring; demuestra el funcionamiento
del software y framework a usar en el curso.

### El servicio acepta peticiones HTTP GET en el EndPoint
```
http://localhost:8080/saludo
```

### Y responde con una representación JSON de un saludo

```
 {"id":1,"respuesta":"Hola Mundo!"}
```

Software requerido
>- JDK 1.8+.
>- Maven 3.0+.
>- Firefox o Chrome.

### 	Construir un JAR ejecutable

Para construir un JAR ejecutable, desde la línea de comando ejecutamos el comando:

```
 mvn compile package
```
### Ejecutar JAR

Una vez generado el JAR, podemos correr la aplicación con:

```
java -jar target/spring-boot-sample-1.0-SNAPSHOT.jar
```
### Correr la aplicación con Maven

Ejecutando el siguiente comando podemos correr la aplicación sin generar el JAR:

```
./mvn spring-boot:run
```
