package ni.com.ucem.sample;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * Spring MVC REST Workflow
 *
 * A continuación se describe el workflow de Spring MVC REST:
 * 
 * 1- El cliente envia una petición al servicio web en forma de URI. 
 * 2- La petición es interceptada por el DispatcherServlet, el cual busca entre los Handler Mappings (@RestController) 
 *    en base a la petición entrante. Spring MVC sporta tres tipos de mapping request URIs a los controladores: 
 *    anotaciones, convenciones de nombre y mappings explicitos. 
 * 3 -Las peticiones son procesadas por el controlador para luego ser retornadas al DispatcherServlet quien finalmente
 *    la envia hacia el response body.
 */
@RestController // DispatcherServlet busca entre las clases definidas con este estereotipo.
@RequestMapping("/saludo") // Indica que la petación HTTP es manejada por el controlador.
public class SaludoCtrl 
{
    @RequestMapping(method=RequestMethod.GET, produces = "application/json")
    public Saludo saludar()
    {
        /*
         * Spring convierte automáticamente el valor retornado y lo escribe en la respuesta http. tras bambalinas Spring
         * tiene una lista de HttpMessageConverters registrados. la responsabilidad del HttpMessageConverters es de 
         * convertir el cuerpo de la petición a una clase específica y de regreso al cuerpo de la respuesta, dependiendo
         * del mime type.
         * 
         * Por otra parte, podemos observar que el EndPoint retorna un DTO y que la respuesta a retornar será de tipo JSON,
         * para ello, Sring hace uso de del procesador de JSON Jackson, el cual es una libreria que mapea objetos java a
         * objetos JSON (serialización).
         */
        return new Saludo("Hola!");
    }
}
