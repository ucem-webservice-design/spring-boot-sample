package ni.com.ucem.sample;

public class Saludo
{
  private final String respuesta;

  public Saludo(final String respuesta)
  {
    this.respuesta = respuesta;
  }

  public String getRespuesta()
  {
    return this.respuesta;
  }
}
